import * as API from './api';
import { currentPage } from './api';

import Spinner from './spinner';

import { favoriteRendered, movieRendered, randomRendered, startPage, favoriteMovies } from './function';

export const render = (): void => {
    const spinner = new Spinner();
    spinner.start();

    window.onload = function() {
        spinner.stop();
    };

    const previousPage = document.querySelector('#previous-page') as HTMLButtonElement;
    const filmContainer = document.querySelector('#film-container') as HTMLElement;
    const favorite = document.querySelector('#favorite-movies') as HTMLElement;
    const nextPage = document.querySelector('#next-page') as HTMLElement;
    const upcoming = document.querySelector('#upcoming') as HTMLElement;
    const topRated = document.querySelector('#top_rated') as HTMLElement;
    const search = document.querySelector('#search') as HTMLInputElement;
    const submit = document.querySelector('#submit') as HTMLDivElement;
    const popular = document.querySelector('#popular') as HTMLElement;

    previousPage.disabled = true;

    startPage();
    randomRendered();
    favoriteRendered();

    filmContainer.addEventListener(
        'click',
        favoriteMovies.bind(this),
    );

    favorite.addEventListener(
        'click',
        favoriteMovies.bind(this),
    );

    popular.addEventListener(
        'click',
        function() {
            API.getPopular()
                .then(data => {
                    window.scroll(0, 0);
                    movieRendered(data);
                });
        },
    );

    nextPage.addEventListener(
        'click',
        function() {
            API.nextPage()
                .then(data => {
                    window.scroll(0, 0);
                    movieRendered(data);

                    if (currentPage > 1) {
                        previousPage.disabled = false;
                    }
                });
        },
    );

    previousPage.addEventListener(
        'click',
        function() {
            API.previousPage()
                .then(data => {
                    window.scroll(0, 0);
                    movieRendered(data);

                    if (currentPage <= 1) {
                        previousPage.disabled = true;
                    }
                });
        },
    );

    upcoming.addEventListener(
        'click',
        function() {
            API.getUpcoming()
                .then(data => {
                    window.scroll(0, 0);
                    movieRendered(data);
                });
        },
    );

    topRated.addEventListener(
        'click',
        function() {
            API.getTopRated()
                .then(data => {
                    window.scroll(0, 0);
                    movieRendered(data);
                });
        },
    );

    submit.addEventListener(
        'click',
        function() {
            API.searchMovies(search.value)
                .then(data => {
                    movieRendered(data);
                });
        },
    );
};
