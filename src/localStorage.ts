import { MovieType } from './types';

export default class LocalStorageUtils {

    getMovies(key = 'favorite'): MovieType[] {
        const moviesLocalStorage = localStorage.getItem(key);
        if (moviesLocalStorage !== null) {
            return JSON.parse(moviesLocalStorage);
        }
        return [];
    }

    findMovie(key: string, id: number): boolean {
        const moviesLocalStorage = this.getMovies(key);
        const index = moviesLocalStorage.findIndex(n => n.id == id);
        return index != -1;
    }

    addMovie(key: string, ...value: MovieType[]): void {
        const moviesLocalStorage = this.getMovies(key);
        moviesLocalStorage.push(...value);
        localStorage.setItem(key, JSON.stringify(moviesLocalStorage));
    }

    removeMovie(key: string, id: number): void {
        const oldList = this.getMovies(key);
        for (let i = 0; i < oldList.length; i++) {
            if (oldList[i].id == id) {
                oldList.splice(i, 1);
            }
        }
        localStorage.clear();
        if (oldList.length > 0) this.addMovie(key, ...oldList);
    }
}
