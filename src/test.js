// https://developer.mozilla.org/ru/docs/Web/API/Fetch_API/Using_Fetch
// Пример отправки GET запроса:
// Как лучше писать ?
async function getData(url = '') {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
    });
    return await response.json(); // parses JSON response into native JavaScript objects
}

getData('https://example.com/answer')
    .then((data) => {
        console.log(data); // JSON data parsed by `response.json()` call
    });

// 2й вариант

function getData(url = '') {
    return fetch(url, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    }).then(data => {
        if (data.ok) {
            return data.json();
        }

        throw new Error('request was rejected by server');
    });
}

getData.then((data) => {
    console.log(data);
});
