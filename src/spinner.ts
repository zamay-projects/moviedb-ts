export default class Spinner {
    private body: HTMLBodyElement;
    private spinner: HTMLDivElement;

    constructor() {
        this.spinner = document.querySelector('.spinner') as HTMLDivElement;
        this.body = document.querySelector('body') as HTMLBodyElement;
    }

    start(): void {
        this.spinner.style.cssText = `
            width: 100%;
            height: 100%;
            background: gray;
            opacity: 0.6;
            color: white;
            position: absolute;
            z-index: 100;
            text-align: center;
            display: block;
          `;
        this.body.style.overflow = 'hidden';
    }

    stop(): void {
        this.spinner.style.display = 'none';
        this.body.style.overflow = 'visible';
    }
}
