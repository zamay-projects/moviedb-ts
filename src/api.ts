import { API_KEY, ENDPOINT } from './apiParams';
import { MovieType } from './types';

/*** Цей коментар для мене
 * lastParamsLeft - щоб зберегти останні параметри / знати де знаходиться юзер. Використовію для пагінації
 * currentItems - використовую, щоб змінювати стиль сердця зразу, а не після перезавантаження сторінки
 * saveParams - костиль, але щоб не зберігати невірні параметри ( '/movie/latest' і '/movie/${id}' )
 *
 * в getData намагаюсь зробити маппер. Не знаю як правильно, але це працює...
 *
 * */

let lastParamsLeft = '';
export let currentPage = 1;
export let currentItems: MovieType[];

const getData = (paramsLeft: string, paramsRight = '', saveParams = true) => {
    let params: string;

    if (saveParams) {
        lastParamsLeft = paramsLeft;
        params = paramsLeft;
    } else {
        params = paramsLeft;
    }

    const lastUrl = `${ENDPOINT}${params}?api_key=${API_KEY}&page=${currentPage}${paramsRight}`;

    return fetch(
        `${lastUrl}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(data => {
            if (data.ok) {
                return data.json();
            }

            throw new Error('request was rejected by server');
        })
        .then(data => {
            const { page, results } = data;
            if (page) {
                currentPage = page;
            }
            if (results) {
                currentItems = results.map((item: MovieType) => {
                    const { title, overview, poster_path, release_date, id } = item;
                    return { title, overview, poster_path, release_date, id };
                });
                return currentItems;
            }
            return data;
        })
        .catch((err) => {
            console.log('getData', err);
            window.location.reload();
        });
};

export const searchMovies = (query: string): Promise<MovieType[]> => {
    let paramsRight = '';

    if (query.length) {
        paramsRight += `&query=${query}`;
    } else {
        return Promise.reject(
            new Error('missing required argument "query"'),
        );
    }

    currentPage = 1;
    return getData('/search/movie', paramsRight);
};

export const getPopular = (): Promise<MovieType[]> => {
    currentPage = 1;
    return getData('/movie/popular');
};

export const getUpcoming = (): Promise<MovieType[]> => {
    currentPage = 1;
    return getData('/movie/upcoming');
};

export const getTopRated = (): Promise<MovieType[]> => {
    currentPage = 1;
    return getData('/movie/top_rated');
};

export const previousPage = (): Promise<MovieType[]> => {
    currentPage -= 1;
    if (!currentPage) {
        currentPage = 1;
    }

    return getData(lastParamsLeft);
};

export const nextPage = (): Promise<MovieType[]> => {
    currentPage += 1;

    return getData(lastParamsLeft);
};

export const getMovie = (id: number): Promise<MovieType | void> => {
    return getData(`/movie/${id}`, '', false)
        .then(item => {
            const { title, overview, poster_path, release_date, id } = item;
            return { title, overview, poster_path, release_date, id };
        })
        .catch((err) => {
            console.log('getUrl', err);
        });
};

export const getRandomFilm = (): Promise<MovieType | void> => {
    return getData('/movie/latest', '', false)
        .then((data) => {
            return data.id;
        })
        .then(max => {
            return Math.floor(Math.random() * max) + 1;
        })
        .then(id => {
            return getData(`/movie/${id}`, '', false);
        })
        .then(item => {
            const { title, overview, poster_path, release_date, id } = item;

            if(!title || !overview ) {
                throw new Error('Title or overview is empty');
            }

            return { title, overview, poster_path, release_date, id };
        })
        .catch((err) => {
            console.log('getRandomFilm', err);
            window.location.reload();
        });
};
