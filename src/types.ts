export type MovieType = {
    title: string,
    overview: string,
    poster_path: string,
    release_date: string,
    id: number
}
