import * as API from './api';
import { currentItems } from './api';

import { MovieType } from './types';

import LocalStorageUtils from './localStorage';

export const startPage = (): void => {
    API.getPopular()
        .then((items): void => {
            movieRendered(items);
        });
};

export const randomRendered = (): void => {
    const container = document.querySelector('#random-movie') as HTMLSelectElement;
    container.innerHTML = '';

    API.getRandomFilm().then(item => {
        if (item) {
            const html = `
            <div class='row py-lg-5'>
                <div class='col-lg-6 col-md-8 mx-auto' style='background-color: #2525254f'>
                    <h1 id='random-movie-name' class='fw-light text-light'>${item.title}</h1>
                    <p id='random-movie-description' class='lead text-white'>${item.overview}</p>
                </div>
            </div>
            `;
            container.insertAdjacentHTML('beforeend', html);
        }
    });
};

export const movieRendered = (listMovies: MovieType[]): void => {
    const container = document.querySelector('#film-container') as HTMLDivElement;
    container.innerHTML = '';
    const ls = new LocalStorageUtils();
    const moviesLS = ls.getMovies();
    const moviesLSId = moviesLS.map(item => item.id);

    for (const movie of listMovies) {
        const html = `
           <div class='col-lg-3 col-md-4 col-12 p-2 movie' data-source='${movie.id}'>
                <div class='card shadow-sm'>
                    <img
                        src='https://image.tmdb.org/t/p/original/${movie.poster_path}'
                        alt='image'/>
                    <svg
                        xmlns='http://www.w3.org/2000/svg'
                        stroke='red'
                        fill='${moviesLSId.includes(movie.id) ? 'red' : '#ff000078'}'
                        width='50'
                        height='50'
                        class='bi bi-heart-fill position-absolute p-2'
                        viewBox='0 -2 18 22'
                    >
                        <path fill-rule='evenodd' d='M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z' />
                    </svg>
                    <div class='card-body'>
                        <p class='card-text truncate'>
                            ${movie.overview}
                        </p>
                        <div class='
                                d-flex
                                justify-content-between
                                align-items-center
                            ' >
                            <small class='text-muted'>${movie.release_date}</small>
                        </div>
                    </div>
                </div>
           </div>
        `;

        container.insertAdjacentHTML('beforeend', html);
    }
};

// Todo: підібрати тип для 'event.target.closest'
export const favoriteMovies = (event: any): void => {
    event.preventDefault();

    if (event.target === event.currentTarget) {
        return;
    }

    const movieCard = event.target.closest('.movie');
    const movieId = movieCard.dataset.source;
    const ls = new LocalStorageUtils();

    if (ls.findMovie('favorite', movieId)) {
        ls.removeMovie('favorite', movieId);
        favoriteRendered();
        movieRendered(currentItems);
    } else {
        API.getMovie(movieId).then(data => {
            if (data) {
                ls.addMovie('favorite', data);
                favoriteRendered();
                movieCard.children[0].children[1].style.fill = 'red';
            }
        });
    }
};

export const favoriteRendered = (): void => {
    const container = document.querySelector('#favorite-movies') as HTMLDivElement;
    const ls = new LocalStorageUtils();
    const listFavoriteMovies = ls.getMovies('favorite');
    container.innerHTML = '';

    for (const item of listFavoriteMovies) {
        const html = `
            <div class='col-12 p-2 movie' data-source='${item.id}'>
                <div class='card shadow-sm'>
                    <img
                        src='https://image.tmdb.org/t/p/original/${item.poster_path}'
                        alt='imageBg'/>
                    <svg
                        xmlns='http://www.w3.org/2000/svg'
                        stroke='red'
                        fill='red'
                        width='50'
                        height='50'
                        class='bi bi-heart-fill position-absolute p-2'
                        viewBox='0 -2 18 22'
                    >
                        <path fill-rule='evenodd'  d='M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z' />
                    </svg>
                    <div class='card-body'>
                        <p class='card-text truncate'>
                            ${item.overview}
                        </p>
                        <div class=' d-flex justify-content-between align-items-center'>
                            <small class='text-muted'>${item.release_date}</small>
                        </div>
                    </div>
                </div>
            </div>
        `;

        container.insertAdjacentHTML('beforeend', html);
    }
};

